<?php
/**
 *格式化TXT文件，去掉
 *用装饰器模式
 *1.去掉空白行--OK
 *2.去掉行首空格--OK
 3.行切割---耗时2个多小时，没有完成。放一段时间再做吧。
 */
abstract class Component{
	abstract public function operation();
	//将数组写入文本文件
	public function saveToFile($fileArr,$filePath,$end=false){
		$handle = fopen($filePath,'wb+');
		if($end !== false){
			$lineNum = count($fileArr);
		}
		//var_dump($fileArr);exit;
		foreach($fileArr as $k=>$line){
			if(($end !== false)&&($k == ($lineNum - 1))){
				fwrite($handle,$line . chr(03));
			}else{
				fwrite($handle,$line . chr(13) . chr(10));
			}
		}
	}
}

class ConcreteComponent extends Component{
	private $filePath;			//文件路径
	
	public function setFilePath($filePath){
		$this->filePath = $filePath;
	}
	
	public function getFilePath(){
		return $this->filePath;
	}
	
	/**
	 *读取文件
	 *@return array $file，文件内容，每个数组元素是一行内容
	 */
	public function operation(){
		$fileArr = array();
		$handle = fopen($this->filePath,'r');
		if($handle){
			while(($buffer = fgets($handle)) !== false){
				$fileArr[] = $buffer;
			}
		}
		fclose($handle);
		return $fileArr;
	}
}

abstract class Decorator extends Component{
	protected $component;
	
	public function setComponent($component){
		
		$this->component = $component;
	}
	
	public function operation(){
		if($this->component != null){
			return $this->component->operation();
		}
	}
}

//去除空白行
class StripEmptyLine extends Decorator{
	const EMPTY_LINE_ASCII_CODE = 13;
	//去掉空白行
	private function stripSpaceLine($fileArr){
		
		$resArr = array();
		foreach($fileArr as $v){
			if(ord($v) === self::EMPTY_LINE_ASCII_CODE){
				continue;
			}
			$resArr[] = $v;
		}
		return $resArr;
	}
	
	public function operation(){
		$fileArr = parent::operation();
		return $this->stripSpaceLine($fileArr);
	}
}

//去除行首的空格
class StripLineFirstEmpty extends Decorator{
	//去掉行首空格
	private function stripFirstSpace($fileArr){
		$resArr = array();
		foreach($fileArr as $v){
			$resArr[] = ltrim($v);
		}
		return $resArr;
	}
	
	public function operation(){
		$fileArr = parent::operation();
		return $fileArr = $this->stripFirstSpace($fileArr);
	}
}

//以所有的文本为整体，平均分行
class CutTxt extends Decorator{
	const CHINESE_CHARACTER_LEN = 3;
	const LINE_MAX_LEN = 100;
	//private $separators = array('，','。','？','：','！');
	private $separators = array('，','。','？','！','：','）','”');
	
	private function cut($line){
		$len = mb_strlen($line);
		if($len > self::LINE_MAX_LEN){
			//切割这一行
			$cutRes = $this->cutLine($line,0);
			$current = $cutRes['current'];
			$remain = $cutRes['remain'];
			$bool = true;
		}else{
			$current = $line;
			$remain = '';
			$bool = false;
		}
		
		$result = array('current'=>$current,
			'remain'=>$remain,
			'bool'=>$bool);
			
		return $result;
	}
	
	private function cutLine($line,$start){
		$childrenLine = array();
		$tempLine = mb_substr($line,$start,self::LINE_MAX_LEN);
		$lastChara = mb_substr($tempLine,-3);
		if(array_search($lastChara,$this->separators)){
			$current = $tempLine;
			$remain = mb_substr($line,$start + self::LINE_MAX_LEN);
		}else{
			$newStart = $start + self::LINE_MAX_LEN;
			$tempRemain = mb_substr($line,$newStart);
			$separatorPos = $this->getLineSeparator($tempRemain);
			$current = mb_substr($line,$start,self::LINE_MAX_LEN + $separatorPos + 3);
			$remain = mb_substr($line,$start + self::LINE_MAX_LEN + $separatorPos + 3);
		}
		
		$result = array('current'=>$current,
			'remain'=>$remain);
		
		return $result;
	}
	
	//获取行分割符
	private function getLineSeparator($remain){
		$separatorsCount = count($this->separators);
		foreach($this->separators as $key=>$value){
			if($separatorPos = strpos($remain,$value))
			{
				break;
			}
		}
		if($separatorPos === false){
			$separatorPos = mb_strlen($remain);
		}
		return $separatorPos;
	}
	
	//
	public function operation(){
		$fileArr = parent::operation();
		if(in_array(chr(03),$fileArr)){
			echo time();exit;
			return $fileArr;
		}
		$resArr = array();
		foreach($fileArr as $line){
			$len = mb_strlen($line);
			$num = ceil($len / self::LINE_MAX_LEN);
			for($i = 0;$i < $num;$i++){
				$cutResArr = $this->cut($line);
				$resArr[] = $cutResArr['current'];
				if(empty($cutResArr['remain'])){
					break;
				}else{
					$line = $cutResArr['remain'];
				}
			}
		}
		return $resArr;
	}
}

class Client{
	public static function main(){
		$filePath = 'test.txt';
		$c = new ConcreteComponent();
		$c->setFilePath($filePath);
		$d1 = new StripEmptyLine();
		$d1->setComponent($c);
		/*
		$fileArr = $d1->operation();
		$d1->saveToFile($fileArr,$filePath);exit;
		*/
		$d2 = new StripLineFirstEmpty();
		$d2->setComponent($d1);
		//$fileArr = $d2->operation();
		//var_dump($fileArr);exit;
		//$d2->saveToFile($fileArr,$filePath);
		
		$d3 = new CutTxt();
		$d3->setComponent($d2);
		$fileArr = $d3->operation();
		//var_dump($fileArr);exit;
		//array_pop($fileArr);
		$d3->saveToFile($fileArr,$filePath,chr(03));
	}
}

//test
Header('Content-Type:text/html;charset=utf-8');
Client::main();
