<?php
//  How to check a variable to see if it can be called
//  as a function.

//
//  Simple variable containing a function
//

function someFunction() 
{
}

$functionVariable = 'someFunction';

var_dump(is_callable($functionVariable, true, $callable_name));  // bool(true)

echo $callable_name, "\n";  // someFunction

//
//  Array containing a method
//

class someClass {

  function someMethod() 
  {
  }

}

$anObject = new someClass();

$methodVariable = array($anObject, 'someMethod');

var_dump(is_callable($methodVariable, false, $callable_name));  //  bool(true)

echo $callable_name, "\n";  //  someClass::someMethod

//test again
function test(){
	
}

var_dump(is_callable('test',false,$callableName));
var_dump($callableName);

//test 
var_dump(is_callable('test2',false,$callableName));
var_dump($callableName);

?> 