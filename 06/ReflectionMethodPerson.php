<?php
class Person {

	/**
	 * For the sake of demonstration, we"re setting this private
	 */
	private $_allowDynamicAttributes = false;

	/**
	 * type=primary_autoincrement      null
	 */
	protected $id = 0;

	/**
	 * type=varchar length=255 'cg'
	 */
	protected $name = 'cg';

	/**
	 * type=text null
	 */
	protected $biography;

	public function getId() {
		return $this->id;
	}

	public function setId($v) {
		$this->id = $v;
	}

	public function getName() {
		return $this->name;
	}

	public function setName($v) {
		$this->name = $v;
	}

	public function getBiography() {
		return $this->biography;
	}

	public function setBiography($v) {
		$this->biography = $v;
	}
	
	public function test(int $a,$b,$c,$d){
		echo __METHOD__;
	}
}

$class = new ReflectionClass('Person'); // 建立 Person这个类的反射类  
//var_dump($class);exit;
$args = array();
$instance  = $class->newInstanceArgs($args); // 相当于实例化Person 类  

$properties = $class->getProperties(ReflectionProperty::IS_PROTECTED);
foreach ($properties as $property) {
	echo $property->getName() . "<br />";
}
var_dump($properties);
foreach ($properties as $property) {
	if ($property->isProtected()) {
		$docblock = $property->getDocComment();
		//var_dump($docblock);
		preg_match('/ type\=([a-z_]*) /', $property->getDocComment(), $matches);
		//var_dump($matches);
		echo $matches[1] . "<br />";
	}
}
// Output:
// primary_autoincrement
// varchar
// text

$instance->getName(); // 执行Person 里的方法getName
// 或者：
$method = $class->getmethod('getName');	// 获取Person 类中的getName方法
$name = $method->invoke($instance);				// 执行getName 方法
var_dump($name);
// 或者：
$method = $class->getmethod('setName');	// 获取Person 类中的setName方法
$method->invokeArgs($instance, array('snsgou.com'));
$method = $class->getmethod('getName');	// 获取Person 类中的getName方法
$name = $method->invoke($instance);				// 执行getName 方法
var_dump($name);

// 执行detail方法
$method = new ReflectionMethod('Person', 'test');
echo $method->class . '<br />';

if ($method->isPublic() && !$method->isStatic()) {
	echo 'Action is right<br />';
}
echo $method->getNumberOfParameters() . '<br />'; // 参数个数
var_dump($method->getParameters()) . '<br />'; // 参数对象数组
