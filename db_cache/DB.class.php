<?php
/**
 *cg
 *2015/01/20 10:36---14：52
 *key-value型的数据库
 *完成了，但性能并不好。原因是：1.每次都读取全部.inx数据，将其清空，然后再将全部.inx数据写入.inx文件。
 *2.删除数据时，只删除了key，没有删除value。.dat文件会越变越大。
 *使用的时候，要注意：存储大段文本时，使用Nowdoc结构。
 */
class DB
{
	private $fp_index;				//存储key的文件的文件资源
	private $fp_dat;				//存储value的文件的文件资源
	private $index_name;			//存储key的文件名
	
	public function __construct($db_name)
	{
		$this->connect($db_name);
	}
	
	
	/**
	 *连接数据库，即是打开key和value的存储文件
	 */
	public function connect($db_name)
	{
		//文件不存在，创建
		$file_index = $db_name . '.inx';
		$file_data = $db_name . '.dat';
		
		$mod = 'w+b';
		
		$this->index_name = $file_index;
				
		//文件存在，打开文件
		$this->fp_index = fopen($file_index,'a+b');
		$this->fp_data = fopen($file_data,'a+b');
	}
	
	/**
	 *hash函数，目的是获得一个字符的唯一hash结果
	 */
	protected function _hash($str)
	{
		return md5($str);
	}
	
	/**
	 *存储数据
	 */
	public function save($key,$value)
	{
		/**
		 *存储key，应该存储什么信息呢？
		 *目的是：根据key，能迅速找到存储在.inx文件中的key和data在.dat文件中的位置信息。
		 *method 1:根据key，直接在.indx文件中查找key;---按这种思路来试一试
		 *method 2:没有方法了。这么干：.indx文件中的所有数据是一个大数组，大数组的每个单元是
		 *一个小数组，小数组的key就是key经过_hash之后的结果，小数组的元素是value的长度和在.dat文件中的offset
		 */
		$key = $this->_hash($key);
		//从.inx中读取index信息，并反序列化
		$key_str = fread($this->fp_index,filesize($this->index_name));
		$arr = unserialize($key_str);
		
		$data_fstat = fstat($this->fp_data);
		$value_offset = $data_fstat['size'];
		$value_length = strlen($value);
		
		$key_arr = array('length'=>$value_length,'offset'=>$value_offset);
		$arr[$key] = $key_arr;
		
		//将value存入.dat文件中，直接用追加mode写入文件末尾即可
		if(fwrite($this->fp_data,$value) === false)			//需要加上写入内容的大小吗？
			return false;
		
		//将key array重新序列化后再写入.inx中
		$key_str = serialize($arr);
		ftruncate($this->fp_index,0);
		if(fwrite($this->fp_index,$key_str) === false)
			return false;
	}
	
	/**
	 *读取数据
	 */
	public function read($key)
	{
		/* var_dump(filesize($this->index_name));
		$key_str = fread($this->fp_index,filesize($this->index_name));
		var_dump(filesize($key_str)); */
		/*此处用上面的代码，为什么不行？*/
		$key_str = file_get_contents($this->index_name);
		$key_arr = unserialize($key_str);
		$key = $this->_hash($key);
		$value_info = $key_arr[$key];
		//根据value的length和offset在.dat文件中读取value
		fseek($this->fp_data,$value_info['offset'],SEEK_SET);
		$value = fread($this->fp_data,$value_info['length']);
		return $value;
	}
	
	/**
	 *删除数据
	 *只能实现从.inx中删除key，不能实现从.dat中删除value（涉及到太多的value的offset)
	 *这样的数据库，很低效
	 */
	public function delete($key)
	{
		$key = $this->_hash($key);
		
		//读取key
		$key_str = file_get_contents($this->index_name);
		$key_arr = unserialize($key_str);
		unset($key_arr[$key]);
		$key_arr = serialize($key_arr);
		ftruncate($this->fp_index,0);
		if(fwrite($this->fp_index,$key_str) === false)
			return false;
		else
			return true;
	}
	
	/**
	 *关闭数据库
	 */
	public function close()
	{
		fclose($this->fp_index);
		fclose($this->fp_data);
	}
}