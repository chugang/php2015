<?php
     class Document
     {
         public $doctype;
         public $head;
         public $title = 'Sensei Ninja';
         public $body;
         private $styles;
         private $metas;
         private $scripts;
         private $document;
         
         
         function __construct (  )
         {
             $this->document = new DOMDocument( );
             $this->head = $this->document->createElement( 'head', ' ' );
             $this->body = $this->document->createElement( 'body', ' ' );
         }
         
         
         public function addStyleSheet ( $url, $media='all' )
         {
             $element = $this->document->createElement( 'link' );
             $element->setAttribute( 'type', 'text/css' );
             $element->setAttribute( 'href', $url );
             $element->setAttribute( 'media', $media );
             $this->styles[] = $element;
         }
         
         
         public function addScript ( $url )
         {
             $element = $this->document->createElement( 'script', ' ' );
             $element->setAttribute( 'type', 'text/javascript' );
             $element->setAttribute( 'src', $url );
             $this->scripts[] = $element;
         }
         
         
         public function addMetaTag ( $name, $content )
         {
             $element = $this->document->createElement( 'meta' );
             $element->setAttribute( 'name', $name );
             $element->setAttribute( 'content', $content );
             $this->metas[] = $element;
         }
         
         
         public function setDescription ( $dec )
         {
             $this->addMetaTag( 'description', $dec );
         }
         
         
         public function setKeywords ( $keywords )
         {
             $this->addMetaTag( 'keywords', $keywords );
         }
         
         public function createElement ( $nodeName, $nodeValue=null )
         {
           return $this->document->createElement( $nodeName, $nodeValue ); 
         }
         
         public function assemble ( )
         {
             // Doctype creation
             $doctype = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML TRANSITIONAL 1.0//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
             
             // Create the head element
             $title = $this->document->createElement( 'title', $this->title );
             // Add stylesheets if needed
             if ( is_array( $this->styles ))
                 foreach ( $this->styles as $element )
                     $this->head->appendChild( $element );
             // Add scripts if needed
             if(  is_array( $this->scripts ))
                 foreach ( $this->scripts as $element )
                     $this->head->appendChild( $element );
             // Add meta tags if needed
             if ( is_array( $this->metas ))
                 foreach ( $this->metas as $element )
                     $this->head->appendChild( $element );
             $this->head->appendChild( $title );
             
             // Create the document
             $html = $this->document->createElement( 'html' );
             $html->setAttribute( 'xmlns', 'http://www.w3.org/1999/xhtml' );
             $html->setAttribute( 'xml:lang', 'en' );
             $html->setAttribute( 'lang', 'en' );
             $html->appendChild( $this->head );
             $html->appendChild( $this->body );
             
             
             $this->document->appendChild( $html );
             return $doctype . $this->document->saveXML( );
         }
         
     }
     
?>