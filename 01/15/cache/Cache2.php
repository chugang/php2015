<?php
class Cache
{
 private $dir = "data/cache/";//定义缓存目录 
 private $key='c_a_sss'; // 文件名md5加密密钥

 function set_dir($dirpath)
 {
  $this->dir=$dirpath;
  $this->make_dir($this->dir);
 }
 function read($key,$minutes=1)
 {
  $filename=$this->get_filename($key);
  if($datas = @file_get_contents($filename))
  {
    $datas = unserialize($datas);
    if(time() - $datas['time'] < $minutes*60)
    {
     return $datas['data'];
    }
  }
  return false;
 }

 function write($key,$data)
 {  
  $filename=$this->get_filename($key);
  if($handle = fopen($filename,'w+'))
  {
   $datas = array('data'=>$data,'time'=>time());
   if(flock($handle,LOCK_EX))
	   echo 1;
   else
	   echo 0;
   $rs = fputs($handle,serialize($datas));
   flock($handle,LOCK_UN);
   fclose($handle);
   if($rs!==false){return true;  }
  }
  return false;
 }
 function clear_all()
 {
  $dir=$this->dir;
  $this->del_file($dir); 
 }

  private function get_filename($key)
 {
  return $this->dir.$key.'_'.md5($key.$this->key);
 }
 private function make_dir($path)
 {
  if (! file_exists ( $path ))
  {
   $this->make_dir ( dirname ( $path ) );
   if (! mkdir ( $path, 0777 ))
   die ( '无法创建缓存文件夹' . $path );
  }
 }
 private function del_file($dir)
 {
  if (is_dir($dir))
  {
   $dh=opendir($dir);//打开目录 //列出目录中的所有文件并去掉 . 和 ..
   while (false !== ( $file = readdir ($dh))) {
    if($file!="." && $file!="..") {
     $fullpath=$dir."/".$file;
     if(!is_dir($fullpath)) {
      unlink($fullpath);
     } else {
      $this->del_file($fullpath);
     }
    }
   }
   closedir($dh);
  }
 }
}

$cache = new cache();
  $cache->set_dir('data/cache_dir/');
  $data=$cache->read('sys2',1);
  if(empty($data))
  {
   $data=array('aa'=>1111,'bb'=>2222,'date'=>date('Y-m-d H:i:s'),'test'=>time());
   $cache->write('sys2',$data); 
  }
  print_r($data);