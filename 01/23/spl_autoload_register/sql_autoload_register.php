<?php
             define('MY_BASE_PATH', (string) (__DIR__ . '/'));
             // Set include path
             $path = (string) get_include_path();
             $path .= (string) (PATH_SEPARATOR . MY_BASE_PATH . 'php_class/');
             $path .= (string) (PATH_SEPARATOR . MY_BASE_PATH . 'php_global_class/');
             // $path .= (string) (PATH_SEPARATOR . 'additional/path/');
             set_include_path($path);

             spl_autoload_register(function ($className) {
                         $className = (string) str_replace('\\', DIRECTORY_SEPARATOR, $className);
                         include_once($className . '.class.php');
                     });
?> 