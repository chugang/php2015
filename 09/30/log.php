<?php
/**
 * 实现Yii的文件日志命名方法
 * 规则是：第一个文件命名为a.log，创建新文件后，新文件名为a.log，第二个文件名为a.log.1。
 * 总之，新建的文件名总是a.log，旧的文件名称依次在后面加上1。而且，超过规定数量的文件会被
 * 删除。
 */
class Log {
	
	protected $maxFiles = 5;
	protected $filename = 'a.log';
	protected $maxFileSize = 1024;			//单位是byte

	/**
	 * 循环处理文件
	 */
	public function rotateFiles ( ) {

		for ( $i = $this->maxFiles; $i > 0; $i-- ) {

			if ( $i == 1 ) {
				$filename = $this->filename;
			} else {
				$filename = $this->filename . '.' . $i;
			}
			
			if ( is_file ( $filename ) ) {
				if ( $i == $this->maxFiles ) {
					unlink ( $filename );
				} else {
					rename ( $filename, $this->filename . '.' . ($i + 1) );
				}
			}
		}
	}

	/**
	 * 创建文件
	 */
	public function createFile ( $logContent ) {
		$fileSize = filesize ( $this->filename );
		if ( $fileSize > $this->maxFileSize * 10 ) {
			$this->rotateFiles ( );
		}

		$fp = fopen ( $this->filename, 'a' );
		fwrite( $fp, $logContent );
		fclose ( $fp );
	}
}

$log = new Log ( );
$logContent = file_get_contents ( 'http://www.hao123.com');
$log->createFile ( $logContent );
